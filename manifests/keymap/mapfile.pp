# Copyright © 2017   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Manage (install or remove) Linux console keymap files.
 *
 * Parameters:
 *  - name		Name under which the keymap file will be installed.
 *			The suffix ".map" will be added to $name.
 *  - ensure		One of 'present' (the default) or 'absent'.
 *  - layouttype	The keyboard layout type which this keymap file
 *			uses.  This governs which subdirectory the file is
 *			installed in; typically something like "sun" or
 *			"i386/azerty".
 *  - source, content	Where the content of the installed keymap file will
 *			come from.  These have the same meanings as the
 *			corresponding parameters to the 'file' type.
 */
define console::keymap::mapfile(
    $ensure='present',
    $source=undef, $content=undef,
    $layouttype='i386/qwerty')
{
    include console::keymap::params
    $mapfile = "${console::keymap::params::keymapdir}/${layouttype}/${name}.map"

    case $ensure
    {
	'present': {
	    if ($source and $content) {
		fail("Console::Keymap::Mapfile[${title}]:",
		     "Must only specify one of source and content")
	    } elsif (! $source and ! $content) {
		fail("Console::Keymap::Mapfile[${title}]:",
		     "Must specify either source or content")
	    }
	    file {
		$mapfile:
		    ensure => file,
		    source => $source, content => $content,
		    owner => 'root', group => 'root', mode => '0444';
	    }
	}
	'absent': {
	    file {
		$mapfile:
		    ensure => absent;
	    }
	}
	default: {
	    fail("Console::Keymap::Mapfile[${title}]:",
		 "Bad value for parameter ensure, `${ensure}'")
	}
    }
}
