# Copyright © 2017-2020   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Define which keymap is to be the default on a Linux console when
 * the machine boots.
 *
 * Parameters:
 *  - keymap		Name of keymap to be set as default.
 *  - source, content	If either of these is set, a keymap file will be
 *			installed as in addition to setting it as the
 *			default keymap, using console::keymap::mapfile.
 *  - layouttype	The keyboard layout type they keymap uses.  This is
 *			only used when $source or $content is specified.
 */
class console::keymap::default(
    $keymap,
    $source=undef, $content=undef,
    $layouttype=undef)
{
    if $source or $content {
	console::keymap::mapfile {
	    $keymap:
		source => $source, content => $content,
		layouttype => $layouttype;
	}
    }

    case "${::osfamily}:${::operatingsystem}:${::initsystem}"
    {
	/^[^:]+:Gentoo:openrc$/: {
	    cfgfile::gentoo::conf_option {
		'console::keymap::default::default_keymap':
		    subsystem => 'keymaps',
		    option => 'keymap', value => $keymap;
	    }
	}

	/^RedHat:[^:]+:systemd$/: {
	    cfgfile::config_option {
		'console::keymap::default::default_keymap':
		    file => '/etc/vconsole.conf',
		    option => 'KEYMAP', value => "\"${keymap}\"",
		    notify => Exec['console::keymap::default::regenerate_initramfs'];
	    }
	    # Console keymap is set before root fs is mounted, so we need to
	    # make sure the initramfs contains the keymap file.
	    exec {
		'console::keymap::default::regenerate_initramfs':
		    command => 'dracut -f',
		    path => ['/sbin', '/bin', '/usr/bin'],
		    refreshonly => true,
		    subscribe => Console::Keymap::Mapfile[$keymap];
	    }
	    # We also need to ascertain that vconsole.keymap is not passed
	    # to the kernel, since that overrides /etc/vconsole.conf.
	    bootloader::kernel_option {
		'vconsole.keymap':
		    ensure => absent;
	    }
	}

	/^RedHat:[^:]+:(sysvinit|upstart)/: {
	    cfgfile::redhat::sysconfig {
		default-keymap:
		    subsystem => 'keyboard',
		    setting => 'KEYTABLE', value => $keymap;
	    }
	}

	default: {
	    fail("Don't know how to set keymap on ${::operatingsystem}.")
	}
    }
}
