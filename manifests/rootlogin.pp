# Copyright © 2019   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Allow or forbid root to login on specific terminals.
 * Manages entries in /etc/securetty.
 *
 * Parameters:
 *  - name	Name of terminal device, without the /dev/ prefix.
 *  - ensure	Whether to 'allow' or 'forbid' root to login on the
 *		terminal in question.
 */
define console::rootlogin($ensure)
{
    case $ensure
    {
	'allow': {
	    augeas {
		"console::serial::login::securetty::${name}":
		    incl => '/etc/securetty',
		    lens => 'Securetty.lns',
		    context => '/files/etc/securetty',
		    changes => "set 099 '${name}'",
		    onlyif => "get *[.='${name}'] != '${name}'";
	    }
	}

	'forbid': {
	    augeas {
		"console::serial::login::securetty::${name}":
		    incl => '/etc/securetty',
		    lens => 'Securetty.lns',
		    context => '/files/etc/securetty',
		    changes => "rm *[.='${name}']";
	    }
	}

	default: {
	    fail("Console::Rootlogin[${title}]: ",
		 "Bad value for parameter ensure, `${ensure}'")
	}
    }
}
