# Copyright © 2017   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Turn off the screensaver on the VGA console.
 * This is useful in case of kernel crashes; if the screensaver was activated
 * at the time of the crash, you can't see the crash message, and you can't
 * deactivate the screensaver, since the kernel has died...
 */
class console::screensaver::disable
{
    # Need redirection to tty1 in case we have console=ttyS1
    $setterm_cmd = 'TERM=linux setterm -blank 0 -powerdown 0 >/dev/tty1'

    case $::initsystem
    {
	'sysvinit', 'upstart': {
	    ensure_line {
		'console::screensaver::disable::setterm':
		    file => '/etc/rc.d/rc.local',
		    line => $setterm_cmd,
		    pattern => '(TERM=\S*\s)?(setterm.*-blank|setterm.*-powerdown).*';
	    }
	}
	'systemd': {
	    systemd::unit {
		'screensaver-disable.service':
		    options => {
			'Unit' => {
			    'Description'=>'Disable screensaver on the console',
			},
			'Service' => {
			    'Type' => 'oneshot',
			    'ExecStart' => "/bin/sh -c '${setterm_cmd}'",
			    'RemainAfterExit' => 'yes',
			},
			'Install' => {
			    'WantedBy' => 'basic.target',
			},
		    },
		    notify => Service['screensaver-disable'];
	    }
	    service {
		'screensaver-disable':
		    enable => true, ensure => running;
	    }
	}
    }
}
