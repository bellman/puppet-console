# Copyright © 2021   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Configuration and default parameter values for console::serial::*
 * classes and definitions.
 */
class console::serial::config(
	# Options to pass to the (a)getty process when logins on a port
	# are enabled.  The default is to follow what the operating
	# system on the node normally does.
	#
	$getty_options = undef,

	# Speed or speeds (in bits/second) getty should use
	#
	$speeds = [ 115200, 38400, 9600 ],

	# Default terminal type connected to ports
	#
	$termtype = 'vt100',


	#####  Parameters specific to console::serial::auto  ###########

	# Ports to enable getty on.  The default is to guess this list; the
	# guessed list will typically only be a single port.
	#
	$enable_ports=undef,

	# Ports to disable getty on.  The default is to compute this as the
	# set difference between $possible_ports and $enable_ports.
	#
	$disable_ports=undef,

	# List of ports where a getty might need to be disabled.  This is
	# only used if $disable_ports is not set.
	#
	$possible_ports=[ 'ttyS0', 'ttyS1', 'hvc0', 'xvc0', ],
)
{
    # Empty class
}
