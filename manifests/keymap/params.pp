# Copyright © 2017   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


class console::keymap::params::default
{
    case "${::operatingsystem}:${::operatingsystemrelease}"
    {
	/^(RedHat|CentOS|Scientific):[56]\.[0-9]+$/: {
	    $keymapdir = '/lib/kbd/keymaps'
	}
	/^(RedHat|CentOS|Scientific):[7]\.[0-9.]+$/: {
	    $keymapdir = '/lib/kbd/keymaps/legacy'
	}
	/^Gentoo:.*$/: {
	    $keymapdir = '/usr/share/keymaps'
	}
	/^Debian:.*$/: {
	    $keymapdir = '/usr/share/keymaps'
	}
    }
}


class console::keymap::params(
    $keymapdir = $console::keymap::params::default::keymapdir,
)
    inherits console::keymap::params::default
{
    if ! $keymapdir {
	fail("Console::Keymap::Params: Directory for keymaps not known")
    }
}
