# Copyright © 2017   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Some systems are configured to suspend the machine when the laptop
 * lid is being closed.  This class disables that behaviour.
 *
 * Currently only implemented for systemd-based systems.
 */
class console::lid::ignore_close
{
    case "${::osfamily}:${::operatingsystem}:${::initsystem}" {
	/^.*:systemd$/: {
	    include console::lid::ignore_close::systemd
	}
	default: {
	    fail("Don't know how to disable suspend on lid close.")
	}
    }
}


# Helper for console::lid::ignore_close

class console::lid::ignore_close::systemd
{
    augeas {
	'console::lid::ignore_close::systemd':
	    incl    => '/etc/systemd/logind.conf',
	    lens    => 'Systemd.lns',
	    context => '/files/etc/systemd/logind.conf',
	    changes => [ 'set Login/HandleLidSwitch/value "ignore"' ],
	    notify  => Service['systemd-logind'];
    }
    service {
	'systemd-logind':
	    enable => true, ensure => running,
	    hasstatus => true, hasrestart => true;
    }
}
