# Copyright © 2018-2021   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Manage login (getty) on serial ports.
 *
 * Parameters:
 *  - name		Name of serial port device (without /dev/ prefix).
 *  - ensure		One of 'enabled' (the default) or 'disabled'.
 *  - rootlogin		To 'allow' or 'forbid' root to login on the port.
 *			Default is to make no changes to /etc/securetty.
 *
 * In addition, the following parameters also exist, and get their
 * default values from console::serial::config.  See that class for
 * the meaning and default values of those parameters.
 *  - getty_options
 *  - speeds
 *  - termtype
 */
define console::serial::login(
	$ensure		= 'enabled',
	$rootlogin	= undef,
	$getty_options	= undef,    # Default from console::serial::config
	$speeds		= undef,    # Default from console::serial::config
	$termtype	= undef,    # Default from console::serial::config
)
{
    case $ensure {
	'enabled', 'disabled': { }
	default: {
	    fail("Console::Serial::Login[${title}]: ",
		 "Bad value for parameter ensure, ``${ensure}''")
	}
    }

    # Get defaults from console::serial::config.
    include console::serial::config
    $x_getty_options = $getty_options ? {
	undef   => $console::serial::config::getty_options,
	default => $getty_options
    }
    $x_speeds = $speeds ? {
	undef   => $console::serial::config::speeds,
	default => $speeds
    }
    $x_termtype = $termtype ? {
	undef   => $console::serial::config::termtype,
	default => $termtype
    }

    case "${::initsystem}::${::operatingsystem}-${::operatingsystemrelease}"
    {
	/systemd::.*/: {
	    console::serial::login::systemd {
		$name:
		    ensure => $ensure,
		    getty_options => $x_getty_options,
		    speeds => $x_speeds,
		    termtype => $x_termtype;
	    }
	}

	/^upstart::(CentOS|Redhat|Scientific)-6(\.|$)/: {
	    console::serial::login::rhel_6 {
		$name:
		    ensure => $ensure,
		    getty_options => $x_getty_options,
		    speeds => $x_speeds,
		    termtype => $x_termtype;
	    }
	}

	default: {
	    fail("Console::Serial::Login[${title}]: ",
		 "Unsupported operating system/init system")
	}
    }

    if ($rootlogin != undef  and  $rootlogin != false)
    {
	console::rootlogin {
	    $name:
		ensure => $rootlogin;
	}
    }
}



# Internal helper definition
#
define console::serial::login::systemd(
	$ensure,
	$getty_options=['--keep-baud'],	# This is the systemd default
	$speeds,
	$termtype,
)
{
    # Systemd cmdline quoting is not *exactly* like shell, but close enough.
    $agetty_cmdline = shellquote(
	# agetty(8) doc says tty name should be before speeds, but default
	# systemd service definition puts %I after; do it like systemd does.
	'/sbin/agetty',
	$getty_options ? { undef => [], default => $getty_options, },
	inline_template('<%= [@speeds].flatten.join(",") %>'),
	'%I',
	$termtype
    )

    systemd::unit_options {
	"serial-getty@${name}.service/param":
	    ensure => $ensure ? {
		'enabled' => 'present', 'disabled' => 'absent',
	    },
	    options => {
		'Service' => {
		    '-ExecStart' => "-${agetty_cmdline}",
		},
	    },
	    notify => Service["serial-getty@${name}"];
    }

    service {
	"serial-getty@${name}":
	    ensure => $ensure ? {
		'enabled' => running, 'disabled' => stopped,
	    },
	    enable => $ensure ? {
		'enabled' => true, 'disabled' => false,
	    },
	    ;
    }
}



# Internal helper definition
#
define console::serial::login::rhel_6(
	$ensure,
	$getty_options=[],
	$speeds,
	$termtype
)
{
    $svcname = "serial-${name}"
    # Upstart uses shell if special characters in command so use shellquote().
    $agetty_cmdline = shellquote(
	'/sbin/agetty',
	$getty_options ? { undef => [], default => $getty_options, },
	"/dev/${name}",
	inline_template('<%= [@speeds].flatten.join(",") %>'),
	$termtype
    )

    if ($ensure == 'enabled')
    {
	include console::serial::login::rhel_6::conflict_auto_serial

	$lines = [
	    'start on stopped rc RUNLEVEL=[2345]',
	    'stop on starting runlevel [016]',
	    'respawn',
	    "exec ${agetty_cmdline}",
	]
	file {
	    "/etc/init/${svcname}.conf":
		ensure => file,
		owner => 'root', group => 'root', mode => '0444',
		content => inline_template(
		    '<%= @lines.join("\n") + "\n" -%>'),
		notify => Exec["console::serial::login::rhel_6::${name}"];
	}
	exec {
	    "console::serial::login::rhel_6::${name}":
		command => shellquote('/sbin/initctl', 'start', $svcname),
		unless => sprintf(
		    '%s | /bin/grep -q "start/running"',
		    shellquote('/sbin/initctl', 'status', $svcname)
		    ),
		path => '/sbin:/usr/sbin:/bin:/usr/bin',
		refreshonly => true;
	}
    }
    elsif ($ensure == 'disabled')
    {
	file {
	    "/etc/init/${svcname}.conf":
		ensure => absent,
		require => Exec["console::serial::login::rhel_6::${name}"];
	}
	exec {
	    "console::serial::login::rhel_6::${name}":
		command => shellquote(
		    '/sbin/initctl', 'stop', $svcname),
		onlyif => sprintf(
		    '%s | /bin/grep -q "start/running"',
		    shellquote('/sbin/initctl', 'status', $svcname)),
		path => '/sbin:/usr/sbin:/bin:/usr/bin';
	}
    }
}


# Internal helper class
# The default /etc/init/serial.conf upstart job runs an agetty process on the
# primary console, if it is a serial port.  That would clash with an agetty
# job configured explicitly on that port.  This class causes that automatic
# agetty job to be stopped if an explicit job for the console port is started.
#
class console::serial::login::rhel_6::conflict_auto_serial
{
    # This is a bit of an abuse of disable_file.  Problem is that older
    # versions of console::serial::login::rhel_6 disabled serial.conf
    # entirely to avoid the conflict, by renaming the file to .DISABLED.
    # Here we un-disable any such file by "disabling" it to its original
    # name...
    disable_file {
	'/etc/init/serial.conf.DISABLED':
	    renameto => '%D/serial.conf',
	    before => Regexp_replace_lines['console::serial::login::rhel_6::conflict_auto_serial'],
	    notify => Exec['console::serial::login::rhel_6::conflict_auto_serial::reload_init'];
    }

    regexp_replace_lines {
	'console::serial::login::rhel_6::conflict_auto_serial':
	    file => '/etc/init/serial.conf',
	    pattern => '^stop on .*',
	    replacement => '\& or starting serial-$DEV',
	    skip => '.* starting serial-\$DEV.*',
	    notify => Exec['console::serial::login::rhel_6::conflict_auto_serial::reload_init'];
    }

    exec {
	'console::serial::login::rhel_6::conflict_auto_serial::reload_init':
	    command => '/sbin/initctl reload-configuration',
	    refreshonly => true;
    }
}
