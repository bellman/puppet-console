# Copyright © 2021   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Try to guess which serial ports to enable getty on, and which to
 * disable on.
 *
 * Parameters:
 *  - enable_ports
 *  - disable_ports
 *  - possible_ports
 *  - getty_options
 *  - speeds
 *  - termtype
 *
 * Default values for these are taken from the console::serial::config class.
 *
 * The intent is that most nodes should be able to do just
 *
 *     include console::serial::auto
 *
 * while nodes needing special handling can do e.g.
 *
 *     class { console::serial::auto: enable_ports => ['ttyS17']; }
 *
 * to override the guessing.
 */
class console::serial::auto(
	$enable_ports	= $console::serial::config::enable_ports,
	$disable_ports	= $console::serial::config::disable_ports,
	$possible_ports	= $console::serial::config::possible_ports,
	$getty_options	= $console::serial::config::getty_options,
	$speeds		= $console::serial::config::speeds,
	$termtype	= $console::serial::config::termtype,
)
     inherits console::serial::config	# To get the params defaults
{
    $osdist = "${::operatingsystem}-${::operatingsystemrelease}"

    if ($enable_ports != undef  and  $enable_ports != false) {
	$x_enableports = $enable_ports
    } elsif ($osdist =~ /^(CentOS|Redhat|Scientific)-6(\.|$)/) {
	$x_enableports = $::virtual ? {
	    'physical'	=> [ 'ttyS0', ],
	    'xen0'	=> [ 'hvc0', ],
	    'xenu'	=> [ 'hvc0', ],
	    'xenhvm'	=> [ 'ttyS0', ],
	    default	=> fail("${name}: Unsupported virtualization"),
	}
    } elsif ($osdist =~ /^(CentOS|Redhat|Scientific)-7(\.|$)/) {
	$x_enableports = $::virtual ? {
	    'physical'	=> [ 'ttyS0', ],
	    'xen0'	=> [ 'hvc0', ],
	    'xenu'	=> [ 'xvc0', ],
	    'xenhvm'	=> [ 'ttyS0', ],
	    default	=> fail("${name}: Unsupported virtualization"),
	}
    } else {
	fail("${name}: Unsupported operating system")
    }

    if ($disable_ports != undef  and $disable_ports != false) {
	$x_disableports = $disable_ports
    } else {
	$x_disableports = split(inline_template(
	    '<%= [@possible_ports].flatten.reject {|port|
		     [@x_enableports].flatten.include?(port)
		 }.join("|")
	     %>'),
	    "[|]")
    }

    console::serial::login {
	$x_enableports:
	    ensure => 'enabled',
	    rootlogin => 'allow',
	    getty_options => $getty_options,
	    speeds => $speeds,
	    termtype => $termtype;

	$x_disableports:
	    ensure => 'disabled', rootlogin => 'forbid';
    }
}
